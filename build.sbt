name := "benchmarks"

version := "1.0-SNAPSHOT"

scalaVersion := "2.10.3"

scalacOptions in ThisBuild ++= Seq(
    "-language:_",
    "-feature",
    "-unchecked",
    "-deprecation")

resolvers ++= Seq(
    "Maven Central" at "http://repo1.maven.org/maven2",
    "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
    "Sonatype Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
    "JBoss Repository" at "http://repository.jboss.org/nexus/content/repositories/releases")

libraryDependencies ++= Seq(
    "org.infinispan" % "infinispan-core" % "5.2.5.Final",
    "com.hazelcast" % "hazelcast" % "3.1",
    "com.google.code.java-allocation-instrumenter" % "java-allocation-instrumenter" % "2.0",
    "com.google.code.gson" % "gson" % "1.7.1",
    "com.google.caliper" % "caliper" % "0.5-rc1")

fork in run := true

javaOptions in run <++= (fullClasspath in Runtime) map { cp => Seq("-cp", sbt.Build.data(cp).mkString(":")) }